package com.ekwateur.billingservice.adapters.output.repositories;

import com.ekwateur.billingservice.adapters.output.repositories.interfaces.ClientJPARepository;
import com.ekwateur.billingservice.domain.BusinessClient;
import com.ekwateur.billingservice.domain.Client;
import com.ekwateur.billingservice.domain.CustomerClient;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.NoSuchElementException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@SpringBootTest
class ClientRepositoryTest {

    @Autowired
    ClientJPARepository repositoryJpa;

    @Autowired
    ClientRepository clientRepository;

    @Test
    void getClientByRefAndConsumptionsWithin_shouldThrowNoSuchElementException_whenClientNotExists() {
        //when/then
        Assertions.assertThrows(NoSuchElementException.class,
                () -> clientRepository.getClientByRefAndConsumptionsWithin("clientNotExistsRef", LocalDate.now(), LocalDate.now().minusMonths(1)));
    }

    @Test
    @Transactional
    void getClientByRefAndConsumptionsWithin_shouldReturnClient_whenClientExistsAndBusiness() {
        //when
        final Client client = clientRepository.getClientByRefAndConsumptionsWithin("EKW11111111", LocalDate.now(), LocalDate.now().minusMonths(1));

        //then
        assertThat(client).isInstanceOf(BusinessClient.class);
    }

    @Test
    @Transactional
    void getClientByRefAndConsumptionsWithin_shouldReturnClient_whenClientExistsAndCustomer() {

        //when
        final Client client = clientRepository.getClientByRefAndConsumptionsWithin("EKW33333333", LocalDate.now(), LocalDate.now().minusMonths(1));

        //then
        assertThat(client).isInstanceOf(CustomerClient.class);
    }
}
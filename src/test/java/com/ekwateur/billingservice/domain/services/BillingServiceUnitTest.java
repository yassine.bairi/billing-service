package com.ekwateur.billingservice.domain.services;

import com.ekwateur.billingservice.domain.BillingDetails;
import com.ekwateur.billingservice.domain.BusinessClient;
import com.ekwateur.billingservice.domain.Client;
import com.ekwateur.billingservice.domain.ClientType;
import com.ekwateur.billingservice.domain.Consumption;
import com.ekwateur.billingservice.domain.EnergyType;
import com.ekwateur.billingservice.ports.output.ClientRepositoryPort;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BillingServiceUnitTest {

    @Mock
    private ClientRepositoryPort clientRepositoryPort;

    @InjectMocks
    private BillingService billingService;

    @Test
    void computeBillingForClient_shouldThrowNoSuchElementException_whenClientNotExists() {
        //given
        when(clientRepositoryPort.getClientByRefAndConsumptionsWithin(any(), any(), any())).thenThrow(NoSuchElementException.class);

        //when/then
        Assertions.assertThrows(NoSuchElementException.class, () -> billingService.computeBillingForClient("refClient"));
    }


    @Test
    void computeBillingForClient_shouldReturnBill_whenEverythingOk() {
        //given

        //given
        final List<Consumption> consumptions = List.of(
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 10L),
                new Consumption(EnergyType.GAS, LocalDate.now(), 7L),
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 15L),
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 9L),
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 8L),
                new Consumption(EnergyType.GAS, LocalDate.now(), 10L),
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 23L),
                new Consumption(EnergyType.GAS, LocalDate.now(), 2L),
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 9L)
        );
        final BillingDetails billingDetails = new BillingDetails(new BigDecimal("0.115"), new BigDecimal("0.112"));
        final Client client = new BusinessClient("ref", ClientType.BUSINESS, consumptions, "siret", "EKW11111111", new BigDecimal("500000"),
                billingDetails);

        when(clientRepositoryPort.getClientByRefAndConsumptionsWithin(any(), any(), any())).thenReturn(client);

        //when
        final BigDecimal billAmount = billingService.computeBillingForClient("refClient");

        //then
        assertEquals(new BigDecimal("10.473"), billAmount);
    }
}
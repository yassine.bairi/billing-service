package com.ekwateur.billingservice.domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class ClientUnitTest {

    @ParameterizedTest
    @CsvSource({
            "0.115, 0.112, 10.473",
            "0.116, 0.111, 10.418",
            "0.114, 0.110, 10.306"
    })
    public void computeBilling_shouldReturnAmountToBill_whenCalled(String gasPrice, String electricityPrice, String expectedBill) {
        //given
        final List<Consumption> consumptions = List.of(
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 10L),
                new Consumption(EnergyType.GAS, LocalDate.now(), 7L),
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 15L),
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 9L),
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 8L),
                new Consumption(EnergyType.GAS, LocalDate.now(), 10L),
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 23L),
                new Consumption(EnergyType.GAS, LocalDate.now(), 2L),
                new Consumption(EnergyType.ELECTRICITY, LocalDate.now(), 9L)
        );
        final BillingDetails billingDetails = new BillingDetails(new BigDecimal(gasPrice), new BigDecimal(electricityPrice));
        final Client client = new BusinessClient("ref", ClientType.BUSINESS, consumptions, "siret", "EKW11111111", new BigDecimal("500000"),
                billingDetails);

        //when
        final BigDecimal amountToBill = client.computeBilling();

        //then
        assertEquals(new BigDecimal(expectedBill), amountToBill);
    }
}
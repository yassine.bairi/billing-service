package com.ekwateur.billingservice.domain;

import java.util.List;

public class CustomerClient extends Client {
    private Long id;
    private Civility civility;
    private String firstName;
    private String lastName;

    public CustomerClient(String ref, ClientType type, List<Consumption> consumptions, Civility civility, String firstName, String lastName, BillingDetails billingDetails) {
        this.ref = ref;
        this.type = type;
        this.consumptions = consumptions;
        this.civility = civility;
        this.firstName = firstName;
        this.lastName = lastName;
        this.billingDetails = billingDetails;
    }
}

package com.ekwateur.billingservice.domain;

public enum EnergyType {
    ELECTRICITY,
    GAS
}

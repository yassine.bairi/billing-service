package com.ekwateur.billingservice.domain;

import java.math.BigDecimal;
import java.util.List;

public class BusinessClient extends Client {
    private String siretNumber;
    private String name;
    private BigDecimal turnover;

    public BusinessClient(String ref, ClientType type, List<Consumption> consumptions, String siretNumber, String name, BigDecimal turnover, BillingDetails billingDetails) {
        this.ref = ref;
        this.type = type;
        this.consumptions = consumptions;
        this.siretNumber = siretNumber;
        this.name = name;
        this.turnover = turnover;
        this.billingDetails = billingDetails;
    }
}

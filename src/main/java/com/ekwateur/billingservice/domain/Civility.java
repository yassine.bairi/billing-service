package com.ekwateur.billingservice.domain;

public enum Civility {
    MADAME,
    MONSIEUR,
}

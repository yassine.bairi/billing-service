package com.ekwateur.billingservice.domain;

public enum ClientType {
    CUSTOMER,
    BUSINESS
}

package com.ekwateur.billingservice.domain;

import java.math.BigDecimal;
import java.util.List;

public abstract class Client {
    protected String ref;
    protected ClientType type;
    protected List<Consumption> consumptions;
    protected BillingDetails billingDetails;

    public BigDecimal computeBilling() {
        BigDecimal totalAmount = BigDecimal.ZERO;

        for (Consumption oneConsumption : consumptions) {
            BigDecimal priceForEnergy = BigDecimal.ZERO;
            if (oneConsumption.getType().equals(EnergyType.ELECTRICITY)) {
                priceForEnergy = billingDetails.getElectricityPrice();
            } else if (oneConsumption.getType().equals(EnergyType.GAS)) {
                priceForEnergy = billingDetails.getGasPrice();
            }

            totalAmount = totalAmount.add(priceForEnergy.multiply(BigDecimal.valueOf(oneConsumption.getAmount())));
        }

        return totalAmount;
    }

}

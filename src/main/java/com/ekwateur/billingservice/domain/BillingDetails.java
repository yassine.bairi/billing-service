package com.ekwateur.billingservice.domain;

import java.math.BigDecimal;

public class BillingDetails {
    private BigDecimal gasPrice;
    private BigDecimal electricityPrice;

    public BillingDetails(BigDecimal gasPrice, BigDecimal electricityPrice) {
        this.gasPrice = gasPrice;
        this.electricityPrice = electricityPrice;
    }

    public BigDecimal getGasPrice() {
        return gasPrice;
    }


    public BigDecimal getElectricityPrice() {
        return electricityPrice;
    }

}

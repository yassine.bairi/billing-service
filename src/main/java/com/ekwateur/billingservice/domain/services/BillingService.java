package com.ekwateur.billingservice.domain.services;

import com.ekwateur.billingservice.domain.Client;
import com.ekwateur.billingservice.ports.input.IBillingService;
import com.ekwateur.billingservice.ports.output.ClientRepositoryPort;

import java.math.BigDecimal;
import java.time.LocalDate;

public class BillingService implements IBillingService {
    private final ClientRepositoryPort repository;

    public BillingService(ClientRepositoryPort repository) {
        this.repository = repository;
    }

    @Override
    public BigDecimal computeBillingForClient(String refClient) {
        final LocalDate startDate = LocalDate.now().minusMonths(1);
        final Client client = repository.getClientByRefAndConsumptionsWithin(refClient, startDate, LocalDate.now());

        return client.computeBilling();
    }
}

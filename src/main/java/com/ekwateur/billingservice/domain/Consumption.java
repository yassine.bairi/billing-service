package com.ekwateur.billingservice.domain;

import java.time.LocalDate;

public class Consumption {
    private EnergyType type;
    private LocalDate date;
    private Long amount;

    public Consumption(EnergyType type, LocalDate date, Long amount) {
        this.type = type;
        this.date = date;
        this.amount = amount;
    }

    public EnergyType getType() {
        return type;
    }

    public Long getAmount() {
        return amount;
    }

}

package com.ekwateur.billingservice.adapters.input;

import com.ekwateur.billingservice.ports.input.IBillingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("/api/billing")
public class BillingResource {
    private final IBillingService billingService;

    public BillingResource(IBillingService billingService) {
        this.billingService = billingService;
    }

    @GetMapping("/{ref}")
    public ResponseEntity<BigDecimal> getBillingForClient(@PathVariable("ref") String ref) {
        return ResponseEntity.ok(billingService.computeBillingForClient(ref));
    }
}

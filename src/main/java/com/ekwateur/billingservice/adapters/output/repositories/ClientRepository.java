package com.ekwateur.billingservice.adapters.output.repositories;

import com.ekwateur.billingservice.adapters.output.repositories.interfaces.ClientJPARepository;
import com.ekwateur.billingservice.domain.Client;
import com.ekwateur.billingservice.ports.output.ClientRepositoryPort;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.NoSuchElementException;

@Repository
public class ClientRepository implements ClientRepositoryPort {
    private final ClientJPARepository repository;

    public ClientRepository(ClientJPARepository repository) {
        this.repository = repository;
    }

    public Client getClientByRefAndConsumptionsWithin(String ref, LocalDate startDate, LocalDate endDate) {
        return repository.findByRefAndConsumptionsDateBetween(ref, startDate, endDate)
                .orElseThrow(NoSuchElementException::new)
                .toDomain();
    }
}

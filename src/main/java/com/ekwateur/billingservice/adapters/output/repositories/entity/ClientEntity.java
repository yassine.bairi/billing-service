package com.ekwateur.billingservice.adapters.output.repositories.entity;

import com.ekwateur.billingservice.domain.BusinessClient;
import com.ekwateur.billingservice.domain.Civility;
import com.ekwateur.billingservice.domain.Client;
import com.ekwateur.billingservice.domain.ClientType;
import com.ekwateur.billingservice.domain.CustomerClient;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Data
@Table(name = "clients")
public class ClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String ref;
    private ClientType clientType;
    private String siretNumber;
    private String name;
    private Civility civility;
    private String firstName;
    private String lastName;
    private BigDecimal turnover;

    @OneToMany
    @JoinColumn(name = "client_id")
    private List<ConsumptionEntity> consumptions;

    @ManyToOne
    @JoinColumn(name = "billing_details_id")
    private BillingDetailsEntity billingDetails;

    public Client toCustomerClient() {
        return new CustomerClient(
                ref,
                clientType,
                consumptions.stream()
                        .map(ConsumptionEntity::toDomain)
                        .collect(Collectors.toList()),
                civility,
                firstName,
                lastName,
                billingDetails.toDomain());
    }

    public Client toBusinessClient() {
        return new BusinessClient(
                ref,
                clientType,
                consumptions.stream()
                        .map(ConsumptionEntity::toDomain)
                        .collect(Collectors.toList()),
                siretNumber,
                name,
                turnover,
                billingDetails.toDomain()
        );
    }

    public Client toDomain() {
        return switch (clientType) {
            case ClientType.BUSINESS -> toBusinessClient();
            case ClientType.CUSTOMER -> toCustomerClient();
        };
    }
}

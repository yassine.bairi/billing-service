package com.ekwateur.billingservice.adapters.output.repositories.entity;


import com.ekwateur.billingservice.domain.Consumption;
import com.ekwateur.billingservice.domain.EnergyType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "consumptions")
public class ConsumptionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private EnergyType type;
    private LocalDate date;
    private Long amount;

    public Consumption toDomain() {
        return new Consumption(type, date, amount);
    }
}

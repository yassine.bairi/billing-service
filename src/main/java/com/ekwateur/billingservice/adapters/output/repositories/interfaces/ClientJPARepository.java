package com.ekwateur.billingservice.adapters.output.repositories.interfaces;

import com.ekwateur.billingservice.adapters.output.repositories.entity.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.Optional;

public interface ClientJPARepository extends JpaRepository<ClientEntity, Long> {

    @Query("SELECT c from ClientEntity c where c.ref= ?1")
    Optional<ClientEntity> findByRefAndConsumptionsDateBetween(String ref, LocalDate startDate, LocalDate endDate);
}

package com.ekwateur.billingservice.adapters.output.repositories.entity;


import com.ekwateur.billingservice.domain.BillingDetails;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

import java.math.BigDecimal;

@Entity
@Data
@Table(name = "billing_details")
public class BillingDetailsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String ref;

    @Column(scale = 3, precision = 3)
    private BigDecimal gasPrice;

    @Column(scale = 3, precision = 3)
    private BigDecimal electricityPrice;

    public BillingDetails toDomain(){
        return new BillingDetails(gasPrice, electricityPrice);
    }
}

package com.ekwateur.billingservice.config;

import com.ekwateur.billingservice.domain.services.BillingService;
import com.ekwateur.billingservice.ports.input.IBillingService;
import com.ekwateur.billingservice.ports.output.ClientRepositoryPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BillingConfiguration {
    @Bean
    public IBillingService billingService(ClientRepositoryPort clientRepositoryPort) {
        return new BillingService(clientRepositoryPort);
    }
}

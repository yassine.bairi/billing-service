package com.ekwateur.billingservice.ports.input;

import java.math.BigDecimal;

public interface IBillingService {
    BigDecimal computeBillingForClient(String refClient);
}

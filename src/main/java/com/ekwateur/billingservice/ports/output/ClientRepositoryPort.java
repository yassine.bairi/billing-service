package com.ekwateur.billingservice.ports.output;

import com.ekwateur.billingservice.domain.Client;

import java.time.LocalDate;

public interface ClientRepositoryPort {
    Client getClientByRefAndConsumptionsWithin(String ref, LocalDate startDate, LocalDate endDate);
}

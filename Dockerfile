FROM amazoncorretto:21-al2-jdk AS build
WORKDIR /app/
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY /src src

RUN ./mvnw -q package -DskipTests

FROM amazoncorretto:21-al2-jdk

COPY --from=build /app/target/**.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar","app.jar"]